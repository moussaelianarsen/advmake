# AdvMake

### Small and simple build system more advanced than make

### Installation

AdvMake can install itself. Simply run `go build` to compile and then `sudo ./advmake install`
to install on Linux, or `sudo ./advmake macos_install` to install on macOS.

### Configuration

AdvMake's build files are written in [Starlark](https://github.com/bazelbuild/starlark) which is
a Python-like language meant for configuration files.

For documentation on build files, read the [Build File Documentation](https://www.arsenm.dev/docs/advmake/build-files).

### Warning

AdvMake's build files are able to execute any command. As with any script, be careful
executing untrusted code, especially as root.