package advmake

import (
	starconv "github.com/sourcegraph/starlight/convert"
	"go.starlark.net/starlark"
)

// Set predeclared values, including previously set builtins
var Predeclared = starlark.StringDict{
	"runtime":  starconv.NewStruct(NewRuntime()),
	"encoding": starconv.NewStruct(NewEncoding()),
	"file":     starconv.NewStruct(NewFile()),
	"strings":  starconv.NewStruct(NewStrings()),
	"input":    starconv.NewStruct(NewInput()),
	"url":      starconv.NewStruct(NewURL()),
	"shell":    starconv.NewStruct(NewShell()),
	"net":      starconv.NewStruct(NewNet()),
	"log":      starconv.NewStruct(NewLog()),
	"fmt":      starconv.NewStruct(NewFmt()),
	"print": starlark.None,
}