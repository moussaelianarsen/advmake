package main

import (
	"fmt"
	"os"

	"gitea.arsenm.dev/Arsen6331/advmake"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	flag "github.com/spf13/pflag"
	"go.starlark.net/starlark"
)

var Log = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

func main() {
	// Create new flag for location of build file
	starlarkFile := flag.StringP("build-file", "f", "AdvMakefile", "Starlark file to use for building")
	// Parse flags
	flag.Parse()

	// Create new starlark thread
	thread := &starlark.Thread{Name: "Build file"}

	predeclared := advmake.Predeclared

	// Execute starlark file with predeclared values
	globals, err := starlark.ExecFile(thread, *starlarkFile, nil, predeclared)
	if err != nil {
		Log.Fatal().Err(err).Msg("Error executing build file")
	}

	// Get arguments
	args := flag.Args()

	// Get default name from starlark globals
	defaultName, _ := starlark.AsString(globals["defaultName"])
	// Get default target from starlark globals
	defaultTarget, _ := starlark.AsString(globals["defaultTarget"])

	// Declare variable for starlark functions
	var starFuncName string
	// Set starFunc
	if len(args) == 0 {
		// If no arguments provided, set starFunc to default function
		starFuncName = fmt.Sprintf("%s_%s", defaultName, defaultTarget)
	} else if len(args) == 1 {
		// If one argument provided, set starFunc to default name, given target
		starFuncName = fmt.Sprintf("%s_%s", defaultName, args[0])
	} else if len(args) == 2 {
		// If 2 arguments provided, set starFunc to given name, given target
		starFuncName = fmt.Sprintf("%s_%s", args[0], args[1])
	}
	// Get function with name derived from arguments
	starFunc := globals[starFuncName]

	// Call given starlark function
	_, err = starlark.Call(thread, starFunc, nil, nil)
	if err != nil {
		Log.Fatal().Str("func", starFuncName).Err(err).Msg("Error calling starlark function")
	}
}
