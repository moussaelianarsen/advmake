package advmake

import (
	"encoding/hex"
	"encoding/json"

	"github.com/pelletier/go-toml"
	"gopkg.in/yaml.v2"
)

type starlarkEncoding struct {
	Json jsonEncoding
	Yaml yamlEncoding
	Toml tomlEncoding
	Hex  hexEncoding
}

type jsonEncoding struct {
	Load func(s string) map[string]interface{}
	Dump func(m map[string]interface{}) string
}

type yamlEncoding struct {
	Load func(s string) map[string]interface{}
	Dump func(m map[string]interface{}) string
}

type tomlEncoding struct {
	Load func(s string) map[string]interface{}
	Dump func(m map[string]interface{}) string
}

type hexEncoding struct {
	Load func(s string) []byte
	Dump func(b []byte) string
}

func NewEncoding() *starlarkEncoding {
	return &starlarkEncoding{
		Json: jsonEncoding{
			Load: jsonLoad,
			Dump: jsonDump,
		},
		Yaml: yamlEncoding{
			Load: yamlLoad,
			Dump: yamlDump,
		},
		Toml: tomlEncoding{
			Load: tomlLoad,
			Dump: tomlDump,
		},
		Hex: hexEncoding{
			Load: hexLoad,
			Dump: hexDump,
		},
	}
}

func jsonLoad(s string) map[string]interface{} {
	jsonMap := make(map[string]interface{})
	_ = json.Unmarshal([]byte(s), &jsonMap)
	return jsonMap
}

func jsonDump(m map[string]interface{}) string {
	jsonData, _ := json.Marshal(m)
	return string(jsonData)
}

func yamlLoad(s string) map[string]interface{} {
	yamlMap := make(map[string]interface{})
	_ = yaml.Unmarshal([]byte(s), &yamlMap)
	return yamlMap
}

func yamlDump(m map[string]interface{}) string {
	yamlData, _ := yaml.Marshal(m)
	return string(yamlData)
}

func tomlLoad(s string) map[string]interface{} {
	tomlMap := make(map[string]interface{})
	_ = toml.Unmarshal([]byte(s), &tomlMap)
	return tomlMap
}

func tomlDump(m map[string]interface{}) string {
	tomlData, _ := toml.Marshal(m)
	return string(tomlData)
}

func hexLoad(s string) []byte {
	data, _ := hex.DecodeString(s)
	return data
}

func hexDump(b []byte) string {
	hexStr := hex.EncodeToString(b)
	return hexStr
}
