package advmake

import (
	"errors"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"

	"go.starlark.net/starlark"
)

type file struct {
	Expand  *starlark.Builtin
	Exists  *starlark.Builtin
	Read *starlark.Builtin
	Write *starlark.Builtin
}

func NewFile() *file {
	return &file{
		Expand:  starlark.NewBuiltin("expandFile", expandFile),
		Exists:  starlark.NewBuiltin("fileExists", fileExists),
		Read: starlark.NewBuiltin("readFile", readFile),
		Write: starlark.NewBuiltin("writeFile", writeFile),
	}
}

// Starlark builtin that expands $VAR instances in a file with provided mappings in the form of a starlark dictionary
func expandFile(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable for file argument from starlark
	var filePath string
	// Declare variable for mappings argument from starlark
	var mappings *starlark.Dict
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "file", &filePath, "mappings", &mappings); err != nil {
		return nil, err
	}
	// Read file from filepath provided
	fileData, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}
	// Expand previously read file
	expandedFileString := os.Expand(string(fileData), func(in string) string {
		// Get value from provided mappings
		val, isFound, err := mappings.Get(starlark.String(in))
		if err != nil {
			// If error, put the variable back in its previous place
			return "$" + in
		}
		if isFound {
			// If value is found, convert it to a go string
			valString, _ := starlark.AsString(val)
			// Return derived go string
			return valString
		} else {
			// If value not found, put variable back in its previous place
			return "$" + in
		}
	})
	// Open file as read/write and truncate
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
	if err != nil {
		return nil, err
	}
	// Close file at the end of function
	defer file.Close()
	// Write expanded string to truncated file
	_, err = file.WriteString(expandedFileString)
	if err != nil {
		return nil, err
	}
	// Return None if successful
	return starlark.None, nil
}

// Starlark builtin that checks if file/files exist
func fileExists(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable for files argument from starlark
	var filePaths starlark.Value
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "files", &filePaths); err != nil {
		return nil, err
	}
	// Check type of object from starlark
	switch filePaths.Type() {
	case "list":
		filePathsList := filePaths.(*starlark.List)
		// Iterate through starlark list
		for i := 0; i < filePathsList.Len(); i++ {
			val, _ := starlark.AsString(filePathsList.Index(i))
			if _, err := os.Stat(val); errors.Is(err, os.ErrNotExist) {
				// If file does not exist, return False
				return starlark.False, nil
			}
		}
	case "string":
		// If starlark type is string, get go string
		filePath, _ := starlark.AsString(filePaths)
		if _, err := os.Stat(filePath); errors.Is(err, os.ErrNotExist) {
			// If file does not exist, return False
			return starlark.False, nil
		}
	}
	// Return True by default
	return starlark.True, nil
}

func readFile(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable for file argument from starlark
	var filePath string
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "file", &filePath); err != nil {
		return nil, err
	}
	content, err := ioutil.ReadFile(filepath.Clean(filePath))
	if err != nil {
		return nil, err
	}
	return starlark.String(content), nil
}

func writeFile(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable for file argument from starlark
	var filePath string
	var content string
	var mode = 0755
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "file", &filePath, "content", &content, "mode?", mode); err != nil {
		return nil, err
	}
	if err := ioutil.WriteFile(filepath.Clean(filePath), []byte(content), fs.FileMode(mode)); err != nil {
		return nil, err
	}
	return starlark.None, nil
}