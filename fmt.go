package advmake

import (
	"fmt"
)

type starlarkFmt struct {
	Println  func(a ...interface{}) (n int, err error)
	Printf   func(format string, a ...interface{}) (n int, err error)
	Print    func(a ...interface{}) (n int, err error)
	Sprintln func(a ...interface{}) string
	Sprintf  func(format string, a ...interface{}) string
	Sprint   func(a ...interface{}) string
	Scanln   func(a ...interface{}) (n int, err error)
	Scanf    func(format string, a ...interface{}) (n int, err error)
	Scan     func(a ...interface{}) (n int, err error)
}

func NewFmt() *starlarkFmt {
	return &starlarkFmt{
		Println:  fmt.Println,
		Printf:   fmt.Printf,
		Print:    fmt.Print,
		Sprintln: fmt.Sprintln,
		Sprintf:  fmt.Sprintf,
		Sprint:   fmt.Sprint,
		Scanln:   fmt.Scanln,
		Scanf:    fmt.Scanf,
		Scan:     fmt.Scan,
	}
}
