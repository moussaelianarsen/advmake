module gitea.arsenm.dev/Arsen6331/advmake

go 1.15

require (
	github.com/cheggaaa/pb/v3 v3.0.5
	github.com/pelletier/go-toml v1.8.1
	github.com/rs/zerolog v1.20.0
	github.com/sourcegraph/starlight v0.0.0-20200510065702-c68e5b848a14
	github.com/spf13/pflag v1.0.5
	go.starlark.net v0.0.0-20210122194613-f935de8d11ef
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	gopkg.in/yaml.v2 v2.4.0
)
