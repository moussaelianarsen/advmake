package advmake

import (
	"fmt"
	"strconv"

	"go.starlark.net/starlark"
)

type input struct {
	Choice *starlark.Builtin
	Prompt *starlark.Builtin
}

func NewInput() *input {
	return &input{
		Choice: starlark.NewBuiltin("userChoice", userChoice),
		Prompt: starlark.NewBuiltin("prompt", prompt),
	}
}

// Starlark builtin that gives users a choice between multiple options
func userChoice(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable for prompt argument from starlark
	var prompt string
	// Declare variable for choices argument from starlark
	var choices *starlark.List
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "prompt", &prompt, "choices", &choices); err != nil {
		return nil, err
	}
	// Iterate through starlark list
	for i := 0; i < choices.Len(); i++ {
		// Print all objects in starlark list with choice number
		fmt.Printf("[%d] %v\n", i+1, choices.Index(i))
	}
	// Print prompt without newline
	fmt.Print(prompt + ": ")
	// Declare variable for user choice index
	var choiceIndex string
	// Get user input
	_, err := fmt.Scanln(&choiceIndex)
	if err != nil {
		return nil, err
	}
	// Convert input to integer
	choiceIndexInt, err := strconv.Atoi(choiceIndex)
	if err != nil {
		return nil, err
	}
	// Return value at chosen index if successful
	return choices.Index(choiceIndexInt - 1), nil
}

// Starlark builtin that gets user input
func prompt(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable for prompt argument from starlark
	var prompt string
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "prompt", &prompt); err != nil {
		return nil, err
	}
	// Print prompt without newline
	fmt.Print(prompt)
	// Delclare variable for user input
	var in string
	// Get user input
	_, err := fmt.Scanln(&in)
	if err != nil {
		return nil, err
	}
	// Return inputted string with no error if successful
	return starlark.String(in), nil
}
