package advmake

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// Set global logger to ConsoleWriter
var internalLog = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

type starlarkLog struct {
	Info  func(msg string)
	Debug func(msg string)
	Warn  func(msg string)
	Fatal func(msg string)
}

func NewLog() *starlarkLog {
	return &starlarkLog{
		Info:  info,
		Debug: debug,
		Warn:  warn,
		Fatal: fatal,
	}
}

func info(msg string) {
	internalLog.Info().Msg(msg)
}

func debug(msg string) {
	internalLog.Debug().Msg(msg)
}

func warn(msg string) {
	internalLog.Warn().Msg(msg)
}

func fatal(msg string) {
	internalLog.Fatal().Msg(msg)
}
