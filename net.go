package advmake

import (
	"errors"
	"io"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strconv"

	"github.com/cheggaaa/pb/v3"
	"go.starlark.net/starlark"
)

type starlarkNet struct {
	Download *starlark.Builtin
}

func NewNet() *starlarkNet {
	return &starlarkNet{
		Download: starlark.NewBuiltin("download", download),
	}
}

// Starlark builtin that downloads a file to an optionally specified filename
func download(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable to store provided URL
	var URL string
	// Declare variable to store optional filename
	var fileName = ""
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "url", &URL, "filename?", &fileName); err != nil {
		return nil, err
	}
	// If filename not provided
	if fileName == "" {
		// Parse provided URL
		parsedURL, err := url.ParseRequestURI(URL)
		if err != nil {
			return nil, err
		}
		// Get filename from parsed URL
		fileName = filepath.Base(parsedURL.Path)
	}
	// GET provided URL
	response, err := http.Get(URL)
	if err != nil {
		return nil, err
	}
	// Close response body at the end of function
	defer response.Body.Close()
	// Declare reader variable to read response
	var reader io.Reader
	// If content length is available
	if response.ContentLength != -1 {
		// Create new progress bar
		progress := pb.Full.Start64(response.ContentLength)
		// Create progress bar proxy reader
		reader = progress.NewProxyReader(response.Body)
		// Close progress bar at the end of function
		defer progress.Finish()
	} else {
		// Set reader directly to response body
		reader = response.Body
	}
	// If request was not successful (non-200 response), return an error
	if response.StatusCode != 200 {
		return nil, errors.New("Received response code " + strconv.Itoa(response.StatusCode))
	}
	// Create new file with set filename
	file, err := os.Create(filepath.Clean(fileName))
	if err != nil {
		return nil, err
	}
	// Close file at the end of function
	defer file.Close()

	// Copy content of response body to file
	_, err = io.Copy(file, reader)
	if err != nil {
		return nil, err
	}
	// Return status code with no error if successful
	return starlark.MakeInt(response.StatusCode), nil
}
