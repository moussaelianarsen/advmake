package advmake

import (
	"runtime"
)

type starlarkRuntime struct {
	GOOS       string
	GOARCH     string
	NumCPU     func() int
	GOMAXPROCS func(i int) int
}

func NewRuntime() *starlarkRuntime {
	run := &starlarkRuntime{
		GOOS:       runtime.GOOS,
		GOARCH:     runtime.GOARCH,
		NumCPU:     runtime.NumCPU,
		GOMAXPROCS: runtime.GOMAXPROCS,
	}
	return run
}
