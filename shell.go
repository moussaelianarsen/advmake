package advmake

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"go.starlark.net/starlark"
	"golang.org/x/sync/errgroup"
)

type shell struct {
	Exec     *starlark.Builtin
	Getenv   func(key string) string
	Setenv   *starlark.Builtin
	LookPath *starlark.Builtin
}

func NewShell() *shell {
	return &shell{
		Exec:     starlark.NewBuiltin("execute", execute),
		Getenv:   os.Getenv,
		Setenv:   starlark.NewBuiltin("setEnv", setEnv),
		LookPath: starlark.NewBuiltin("lookPath", lookPath),
	}
}

// Starlark builtin that executes a command using the shell
func execute(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var err error
	// Declare variable for command argument from starlark
	var command string
	var output = "both"
	var isConcurrent = false
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "command", &command, "output?", &output, "concurrent?", &isConcurrent); err != nil {
		return nil, err
	}
	outBuffer := bytes.NewBuffer(nil)
	// Create a slice of io.Writer to which the command will output
	var execWriter io.Writer
	// Set io.Writer slice based on starlark arguments
	switch output {
	case "stdout":
		// If no output argument given, only write to STDOUT
		execWriter = os.Stdout
	case "return":
		// If output="return", only write to output buffer
		execWriter = outBuffer
	case "both":
		// If output="both", write to both STDOUT and output buffer
		execWriter = io.MultiWriter(os.Stdout, outBuffer)
	}
	if isConcurrent {
		// If concurrency enabled, create new error group
		errs, _ := errgroup.WithContext(context.Background())
		// Split input into individual lines
		splitCommand := strings.Split(command, "\n")
		// For each line
		for _, line := range splitCommand {
			// If the line is a comment
			if strings.HasPrefix(line, "#") {
				// Skip
				continue
			}
			// Set command to run line using sh
			concurrentCommand := exec.Command("sh", "-c", line)
			// Set process I/O
			concurrentCommand.Stdout = execWriter
			concurrentCommand.Stderr = os.Stderr
			concurrentCommand.Stdin = os.Stdin
			// Spawn new goroutine to run concurrent line
			errs.Go(concurrentCommand.Run)
		}
		// Wait for completion of goroutine
		err = errs.Wait()
		if err != nil {
			return nil, err
		}
	} else {
		// Create command using `sh -c` if concurrency disabled
		execCmd := exec.Command("sh", "-c", command)
		// Set process I/O
		execCmd.Stdout = execWriter
		execCmd.Stderr = os.Stderr
		execCmd.Stdin = os.Stdin
		// Run command
		err = execCmd.Run()
		if err != nil {
			return nil, err
		}
	}
	// Get bytes from output buffer
	outBytes, err := ioutil.ReadAll(outBuffer)
	if err != nil {
		return nil, err
	}
	// Return trimmed string from output buffer if successful
	return starlark.String(strings.TrimSpace(string(outBytes))), nil
}

// Starlark builtin that sets the value of an environment variable
func setEnv(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable for key argument from starlark
	var key string
	// Declare variable for value argument from starlark
	var val starlark.Value
	var onlyIfUnset = false
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "key", &key, "val", &val, "onlyIfUnset?", &onlyIfUnset); err != nil {
		return nil, err
	}
	if onlyIfUnset && os.Getenv(key) == "" || !onlyIfUnset {
		// Remove quotes from starlark's repr output
		valStr := strings.TrimPrefix(strings.TrimSuffix(val.String(), `"`), `"`)
		// Set environment variable
		if err := os.Setenv(key, valStr); err != nil {
			return nil, err
		}
	}
	// Return None if successful
	return starlark.None, nil
}

// Starlark builtin that looks for a command in PATH
func lookPath(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable for command name argument from starlark
	var cmdName string
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "cmdName", &cmdName); err != nil {
		return nil, err
	}
	// Check PATH for command
	path, err := exec.LookPath(cmdName)
	// Error most likely means file not found, so return -1 with no error
	if err != nil {
		return starlark.MakeInt(-1), nil
	}
	// Return located path with no error if successful
	return starlark.String(path), nil
}
