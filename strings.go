package advmake

import (
	"os"
	"regexp"
	"strconv"
	"strings"

	"go.starlark.net/starlark"
)

type starlarkStrings struct {
	Regex      *starlark.Builtin
	HasSuffix  func(s string, suffix string) bool
	HasPrefix  func(s string, prefix string) bool
	TrimSuffix func(s string, suffix string) string
	TrimPrefix func(s string, prefix string) string
	TrimSpace  func(s string) string
}

func NewStrings() *starlarkStrings {
	return &starlarkStrings{
		Regex:      starlark.NewBuiltin("regex", regex),
		HasSuffix:  strings.HasSuffix,
		HasPrefix:  strings.HasPrefix,
		TrimSuffix: strings.TrimSuffix,
		TrimPrefix: strings.TrimPrefix,
		TrimSpace:  strings.TrimSpace,
	}
}

func regex(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	var str string
	var pattern string
	var regex string
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "string", &str, "pattern", &pattern, "regex", &regex); err != nil {
		return nil, err
	}
	rgx, err := regexp.Compile(regex)
	if err != nil {
		return nil, err
	}
	match := rgx.FindStringSubmatch(str)
	retPattern := os.Expand(pattern, func(in string) string {

		inputInt, err := strconv.Atoi(in)
		if err == nil && inputInt != 0 {
			if len(match) <= inputInt {
				return ""
			}
			return match[inputInt]
		}

		subIndex := rgx.SubexpIndex(in)
		if subIndex != -1 {
			return match[subIndex]
		}

		if in == "match" {
			if len(match) == 0 {
				return ""
			}
			return match[0]
		}

		return "$" + in
	})
	return starlark.String(retPattern), nil
}
