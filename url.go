package advmake

import (
	"net/url"

	sconv "github.com/sourcegraph/starlight/convert"
	"go.starlark.net/starlark"
)

type starlarkURL struct {
	Parse *starlark.Builtin
}

func NewURL() *starlarkURL {
	return &starlarkURL{
		Parse: starlark.NewBuiltin("parseURL", parseURL),
	}
}

func parseURL(_ *starlark.Thread, builtin *starlark.Builtin, args starlark.Tuple, kwargs []starlark.Tuple) (starlark.Value, error) {
	// Declare variable to store provided URL
	var URL string
	// Get arguments from starlark
	if err := starlark.UnpackArgs(builtin.Name(), args, kwargs, "url", &URL); err != nil {
		return nil, err
	}
	parsedURL, err := url.ParseRequestURI(URL)
	if err != nil {
		return nil, err
	}

	return sconv.NewStruct(parsedURL), err
}
